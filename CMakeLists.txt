﻿cmake_minimum_required (VERSION 3.8)

project (UnitTestTemplateProject)

# Include sub-projects.
add_subdirectory(Main)
add_subdirectory(Test)

set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wall")