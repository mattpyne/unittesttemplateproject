#include "VMath.h"

static const float EPS = 1E-6f;

bool VMath::isEqual(float f1, float f2) {
	float diff = f1 - f2;

	if (diff < 0) 
	{
		return -diff < EPS;
	}
	else 
	{
		return diff < EPS;
	}
}

bool VMath::isEqual(double f1, double f2) {
	double diff = f1 - f2;

	if (diff < 0)
	{
		return -diff < EPS;
	}
	else
	{
		return diff < EPS;
	}
}

