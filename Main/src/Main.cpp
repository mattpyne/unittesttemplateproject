﻿#include "Main.h"
#include "VMath.h"

using namespace std;

int main()
{
	cout << "Hello CMake." << endl;
	cout << "5 == 5.001 " << VMath::isEqual(5.f, 5.00000001f) << endl;
	return 0;
}
