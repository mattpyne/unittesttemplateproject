#pragma once

class VMath {
public:
	static bool isEqual(float d1, float d2);
	static bool isEqual(double d1, double d2);
};
