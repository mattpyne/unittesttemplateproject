#include "Assert.h"
#include "Test.h"
#include "VMathTests.h"

void testEquals1()
{
	Assert::assertTrue(VMath::isEqual(1., 1.));
}

void testNotEquals() 
{
	Assert::assertFalse(VMath::isEqual(-1., 1.));
}

void VMathTests::run() 
{
	Test::run(testEquals1);
	Test::run(testNotEquals);
}