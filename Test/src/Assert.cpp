#include "Assert.h"
#include "VMath.h"
#include <sstream>

void Assert::coutAssertFailed(string assertMessage) 
{
	cout << "\033[1;31mfailed: \033[0m" << assertMessage << endl;
}

void Assert::assertTrue(bool B)
{
	if (!B)
	{
		coutAssertFailed("Expected true but was false");
		throw Assert::AssertFailedException;
	}
}

void Assert::assertTrue(string assertMessage, bool B) 
{
	if (!B) 
	{
		coutAssertFailed(assertMessage);
		throw Assert::AssertFailedException;
	}
}

void Assert::assertFalse(bool B)
{
	if (B)
	{
		coutAssertFailed("Expected false but was true");
		throw Assert::AssertFailedException;
	}
}

void Assert::assertFalse(string assertMessage, bool B)
{
	if (B)
	{
		coutAssertFailed(assertMessage);
		throw Assert::AssertFailedException;
	}
}

void Assert::assertEquals(float expected, float actual)
{
	if (!VMath::isEqual(expected, actual))
	{
		ostringstream myString;
		myString << "Expected " << expected << " but was actually " << actual;
		coutAssertFailed(myString.str());
		throw Assert::AssertFailedException;
	}
}

void Assert::assertEquals(string assertMessage, float expected, float actual) 
{
	if (!VMath::isEqual(expected, actual))
	{
		coutAssertFailed(assertMessage);
		throw Assert::AssertFailedException;
	}
}

void Assert::assertEquals(double expected, double actual)
{
	if (!VMath::isEqual(expected, actual))
	{
		ostringstream myString;
		myString << "Expected " << expected << " but was actually " << actual;
		coutAssertFailed(myString.str());
		throw Assert::AssertFailedException;
	}
}

void Assert::assertEquals(string assertMessage, double expected, double actual)
{
	if (!VMath::isEqual(expected, actual))
	{
		coutAssertFailed(assertMessage);
		throw Assert::AssertFailedException;
	}
}

void Assert::assertEquals(string expected, string actual)
{
	if (expected.compare(actual) != 0)
	{
		ostringstream myString;
		myString << "Expected " << expected << " but was actually " << actual;
		coutAssertFailed(myString.str());
		throw Assert::AssertFailedException;
	}
}

void Assert::assertEquals(string* expected, string* actual)
{
	assertEquals(*expected, *actual);
}

void Assert::assertEquals(string assertMessage, string expected, string actual)
{
	if (expected.compare(actual) != 0)
	{
		coutAssertFailed(assertMessage);
		throw Assert::AssertFailedException;
	}
}

void Assert::assertNotEquals(float cannotBe, float actual)
{
	if (VMath::isEqual(cannotBe, actual))
	{
		ostringstream myString;
		myString << "Should not equal " << cannotBe << " but did.";
		coutAssertFailed(myString.str());
		throw Assert::AssertFailedException;
	}
}

void Assert::assertNotEquals(string assertMessage, float cannotBe, float actual)
{
	if (VMath::isEqual(cannotBe, actual))
	{
		coutAssertFailed(assertMessage);
		throw Assert::AssertFailedException;
	}
}

void Assert::assertNotEquals(double cannotBe, double actual)
{
	if (VMath::isEqual(cannotBe, actual))
	{
		ostringstream myString;
		myString << "Should not equal " << cannotBe << " but did.";
		coutAssertFailed(myString.str());
		throw Assert::AssertFailedException;
	}
}

void Assert::assertNotEquals(string assertMessage, double cannotBe, double actual)
{
	if (VMath::isEqual(cannotBe, actual))
	{
		coutAssertFailed(assertMessage);
		throw Assert::AssertFailedException;
	}
}

void Assert::assertNotEquals(string cannotBe, string actual)
{
	if (cannotBe.compare(actual) == 0)
	{
		ostringstream myString;
		myString << "Should not equal " << cannotBe << " but did.";
		coutAssertFailed(myString.str());
		throw Assert::AssertFailedException;
	}
}

void Assert::assertNotEquals(string assertMessage, string cannotBe, string actual)
{
	if (cannotBe.compare(actual) == 0)
	{
		coutAssertFailed(assertMessage);
		throw Assert::AssertFailedException;
	}
}
