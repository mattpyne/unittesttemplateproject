﻿#include "AssertTests.h"
#include "Test.h"
#include "VMathTests.h"

using namespace std;

int main()
{
	Test::run(new AssertTests);

	Test::run(new VMathTests);	

	Test::coutResults();

	return 0;
}
