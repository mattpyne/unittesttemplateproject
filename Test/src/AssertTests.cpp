#include "Assert.h"
#include "AssertTests.h"
#include "Test.h"

void testAssertTrueFalse()
{
	Assert::assertTrue("testing false is not true", false);
}

void testAssertTrueTrue()
{
	Assert::assertTrue("testing true is true", true);
}

void testAssertEqualsFloatsFalse()
{
	Assert::assertEquals("5 does not equal 5.001", 5.f, 5.001f);
}

void testAssertEqualsFloatsTrue()
{
	Assert::assertEquals("5 equals 5.00000001", 5.f, 5.00000001f);
}

void AssertTests::run()
{
	Test::run(testAssertTrueFalse);
	Test::run(testAssertTrueTrue);

	Test::run(testAssertEqualsFloatsFalse);
	Test::run(testAssertEqualsFloatsTrue);
}