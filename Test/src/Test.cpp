#include "Test.h"
#include "TestSuit.h"

static int TESTS_RAN = 0;
static int TESTS_FAILED = 0;

void Test::run(void (*testMethod)())
{
	try
	{
		TESTS_RAN++;

		(*testMethod)();
	}
	catch (int n)
	{
		(void)n;
		TESTS_FAILED++;
	}
}

void Test::run(TestSuit* suit) {
	suit->run();
}

void Test::coutResults()
{
	cout << endl << endl;
	cout << "=========================================================================" << endl;
	cout << "==============================++RESULTS++================================" << endl;
	cout << "=========================================================================" << endl << endl << endl;
	cout << "Number of tests:         " << TESTS_RAN << endl;
	cout << "\033[1;32mNumber of passing tests: \033[0m" << TESTS_RAN - TESTS_FAILED << endl;
	cout << "\033[1;31mNumber of failed tests:  \033[0m" << TESTS_FAILED << endl;
}