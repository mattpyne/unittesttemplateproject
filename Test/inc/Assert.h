#pragma once

#include <iostream>

using namespace std;

class Assert {
public:

	static const int AssertFailedException = 321123;

	static void assertTrue(bool b);
	static void assertTrue(string assertMessage, bool b);

	static void assertFalse(bool b);
	static void assertFalse(string assertMessage, bool b);

	static void assertEquals(float expected, float actual);
	static void assertEquals(string assertMessage, float expected, float actual);

	static void assertEquals(double expected, double actual);
	static void assertEquals(string assertMessage, double expected, double actual);

	static void assertEquals(string expected, string actual);
	static void assertEquals(string* expected, string* actual);
	static void assertEquals(string assertMessage, string expected, string actual);

	static void assertNotEquals(float cannotBe, float actual);
	static void assertNotEquals(string assertMessage, float cannotBe, float actual);

	static void assertNotEquals(double cannotBe, double actual);
	static void assertNotEquals(string assertMessage, double cannotBe, double actual);

	static void assertNotEquals(string cannotBe, string actual);
	static void assertNotEquals(string assertMessage, string cannotBe, string actual);

private:

	static void coutAssertFailed(string assertMessage);

};