#pragma once

class TestSuit {
public:
	virtual void run() = 0;
};
