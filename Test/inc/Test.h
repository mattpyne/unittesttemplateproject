#pragma once

#include <iostream>
#include "TestSuit.h"

using namespace std;

class Test {
public:

	static void run(void (*testMethod)());
	static void run(TestSuit* testSuit);
	static void coutResults();

};
